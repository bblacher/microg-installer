# microG-installer

This script installs microG GmsCore, microG Companion (former FakeStore) and microG GsfProxy to /system/priv-app

This has been tested with an OnePlus 7T Pro on LineageOS on a build that includes the signature spoofing patches (https://review.lineageos.org/c/LineageOS/android_frameworks_base/+/383573)

Only use this if you know what you are doing, this could get your device stuck in a bootloop, in the worst case you'd have to reflash LineageOS if you don't know how to recover from a bootloop.

Make sure to enable rooted usb debugging in the developer options, run "adb devices" and check if it says "XXXXXXXX device" before running this script.

Make sure you have the following files in the same directory as this script:
- com.android.vending-XXXXXXXX.apk (from https://github.com/microg/GmsCore/releases)
- GsfProxy.apk (from https://github.com/microg/GsfProxy/releases)
- com.google.android.gms-XXXXXXXXX.apk (from https://github.com/microg/GmsCore/releases)

After running this script make sure to add the microG F-Droid repo to your F-Droid/F-Droid Basic (https://microg.org/fdroid.html)