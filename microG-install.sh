#!/bin/bash
#
# Author: Benjamin Blacher (https://gitlab.com/bblacher)
# This script installs microG GmsCore, microG Companion (former FakeStore) and microG GsfProxy to /system/priv-app
# This has been tested with an OnePlus 7T Pro on LineageOS on a build that includes the signature spoofing patches (https://review.lineageos.org/c/LineageOS/android_frameworks_base/+/383573)
# Only use this if you know what you are doing, this could get your device stuck in a bootloop, in the worst case you'd have to reflash LineageOS if you don't know how to recover from a bootloop.
#
# Make sure to enable rooted usb debugging in the developer options, run "adb devices" and check if it says "XXXXXXXX device" before running this script.
#
# Make sure you have the following files in the same directory as this script:
# 	- com.android.vending-XXXXXXXX.apk (from https://github.com/microg/GmsCore/releases)
# 	- GsfProxy.apk (from https://github.com/microg/GsfProxy/releases)
# 	- com.google.android.gms-XXXXXXXXX.apk (from https://github.com/microg/GmsCore/releases)
#
# After running this script make sure to add the microG F-Droid repo to your F-Droid/F-Droid Basic (https://microg.org/fdroid.html)

# restart adb as root
adb root

# remount the filesystem as rw
echo "remounting the filesystem as RW..."
adb remount

# push microg Companion to /system/priv-app/
echo "installing microG Companion to /system/priv-app..."
adb push ./com.android.vending* /system/priv-app/microG-companion/microG-companion.apk

# push microg GsfProxy to /system/priv-app/
echo "installing microG GsfProxy to /system/priv-app..."
adb push ./GsfProxy.apk /system/priv-app/microG-GsfProxy/microG-GsfProxy.apk

# push microg GmsCore to /system/priv-app/
echo "installing microG GmsCore to /system/priv-app..."
adb push ./com.google.android.gms* /system/priv-app/microG-GmsCore/microG-GmsCore.apk

# install microG Companion as a user app (will be overwritten on the next reboot by the priv-app)
# this is needed because we have to set a permission for microG Companion
echo "installing microG Companion as an user app temporarily..."
adb install ./com.android.vending*
echo "setting permissions for microG Companion..."
adb shell pm grant com.android.vending android.permission.FAKE_PACKAGE_SIGNATURE 2>/dev/null

# install microG GmsCore as a user app and grant all runtime permissions
echo "installing microg GmsCore as an user app temporarily and setting permissions..."
adb install -g ./com.google.android.gms*

# reboot the device
echo "rebooting..."
adb reboot

echo "Done!"
